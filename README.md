# phpbb2nodebb

This small tool is supposed to migrate the entire phpbb database into a nodebb database. Changes made throught extensions will probably never be supported in the future.

# TODO (by prio)
1. Find an adequate language for the task (Something lightweight)
2. Extract phpbb tables and write them into nodebb (MySQL -> MongoDB)
3. Find a way to redirect from old urls to new structure (SEO)
4. Recovering all images and uploads
5. Transfere all PMs into chats

# Nice To Have
1. Keep user groups & moderators
 
